/**
 * The User site router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Libraries
 */
const moment = require('moment');
moment.locale('cs');
const numberFormat = require('../libs/numberFormat');

/**
 * Controllers
 */
const errorController = require('../controllers/error');
const userController = require('../controllers/user');
const pageController = require('../controllers/page');

/**
 * Partials methods
 */
const partials = require('./partials');

/**
 * Routes
 */

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment: moment,
        numberFormat: numberFormat,
    };

    // move to the next route
    next();
});

// redirect from / to the login page
router.get('/', (req, res) => {
    res.redirect('/login');
});

// get login page (display it)
router.get('/login', partials.loggedIn, (req, res) => {
    pageController.loginPage(req, res);
});

// post login page (do the login)
router.post('/login', partials.loggedIn, (req, res) => {
    userController.login(req, res);
});

router.get('/register', partials.loggedIn, (req, res) => {
    pageController.registerPage(req, res);
});

router.post('/register', partials.loggedIn, (req, res) => {
    userController.registerMe(req, res);
});

// This row block access without logging in
router.all('*', partials.loginControl);

/**
 * Logout from game
 */
router.get('/logout', (req, res) => {
    userController.logout(req, res);
});


/**
 * Not found the rerquested path
 */
router.all('*', (req, res) => {
    errorController.error404(req, res);
});

/**
 * Export the router
 */
module.exports = router;