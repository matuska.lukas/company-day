/**
 * The admin router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Libraries
 */
const moment = require('moment');
moment.locale('cs');
const numberFormat = require('../libs/numberFormat');

/**
 * Controllers
 */
const adminPageController = require('../controllers/admin/page');
const errorController = require('../controllers/error');
const pageController = require('../controllers/page');
const userController = require('../controllers/user');
const companyController = require('../controllers/company');
const emailController = require('../controllers/admin/email');
const yearController = require('../controllers/admin/year');

/**
 * Partials methods
 */
const partials = require('./partials');

/**
 * Routes
 */

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment: moment,
        numberFormat: numberFormat,
    };

    // move to the next route
    next();
});

// Redirect from /admin to the login page or to dashboard
router.get('/', partials.loggedIn, (req, res) => {
    res.redirect('/admin/dashboard');
});

// Display login page
router.get('/login', partials.loggedIn, (req, res) => {
    adminPageController.login(req, res);
});

// post login page (do the login)
router.post('/login', partials.loggedIn, (req, res) => {
    userController.login(req, res);
});

router.get('/register', partials.loggedIn, (req, res) => {
    adminPageController.register(req, res);
});

router.post('/register', partials.loggedIn, (req, res) => {
    userController.registerMe(req, res);
});

router.get('/forgot-password', partials.loggedIn, (req, res) => {
    adminPageController.forgotPassword(req, res);
});

router.post('/forot-password', partials.loggedIn, (req, res) => {
    userController.forgotPassword(req, res);
});

/**
 * This row block access without logging in
 */ 
router.all('/*', partials.loginControl);

// This row block access without admin permissions
router.all('/*', partials.filterNonAdmin);

/**
 * Dashboard
 */

router.get('/dashboard/', (req, res) => {
    adminPageController.dashboard(req, res);
});

/**
 * Companies
 */
router.get('/companies', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'companies-list');
}, (req, res) => {
    res.redirect('/admin/companies/list');
});

router.get('/companies/list', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'companies-list');
}, (req, res) => {
    companyController.list(req, res);
});


/**
 * Users
 */
router.get('/users', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-list');
}, (req, res) => {
    res.redirect('/admin/users/list');
});

router.get('/users/list', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-list');
}, (req, res) => {
    userController.list(req, res);
});

router.get('/users/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-new');
}, (req, res) => {
    adminPageController.newUser(req, res);
});

router.post('/users/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-new');
}, (req, res) => {
    userController.registerMe(req, res);
});

router.get('/users/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-edit');
}, (req, res) => {
    if (req.query.id) {
        userController.showEdit(req, res);
    } else {
        res.send('not-send-id');
        //next();
    }
});

router.post('/users/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-edit');
}, (req, res) => {
    userController.edit(req, res);
});

router.post('/users/set-new-password', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-change-password');
}, (req, res) => {
    userController.setNewPassword(req, res);
});

router.post('/users/delete', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-delete');
}, (req, res) => {
    userController.delete(req, res);
});

/**
 * Emails
 */

router.get('/email/write', partials.filterByPermissions, (req, res) => {
    res.render('admin/email/write', { req, res });
});

router.post('/email/write', partials.filterByPermissions, (req, res) => {
    emailController.sendEmail(req, res);
});

/**
 * Years
 */

router.post('/year', (req, res) => {
    // this method switches the current (editing) year of user
    if (req.body.year) {
        req.session.year = req.body.year;
        res.send('ok');
    } else {
        res.send('not-sent-year');
    }
});

router.get('/years', partials.filterByPermissions, (req, res) => {
    res.redirect('/admin/years/list');
});

router.get('/years/list', partials.filterByPermissions, (req, res) => {
    yearController.list(req, res);
});

router.get('/years/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-new');
}, (req, res) => {
    yearController.showNew(req, res);
});

router.post('/years/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-new');
}, (req, res) => {
    yearController.new(req, res);
});

router.get('/years/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-edit');
}, (req, res) => {
    yearController.showEdit(req, res);
});

router.post('/years/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-edit');
}, (req, res) => {
    yearController.edit(req, res);
});


/**
 * Logout
 */
router.get('/logout', (req, res) => {
    userController.logout(req, res);
});


/**
 * Not found the rerquested path
 */
router.all('*', (req, res) => {
    errorController.error404admin(req, res);
});

/**
 * Export the router
 */
module.exports = router;