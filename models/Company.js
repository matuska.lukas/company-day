/**
 * Company database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db');
const moment = require('moment');
moment.locale('cs');

// the schema itself
var companySchema = new mongoose.Schema({
    name: {
        type: String,
    },
    address: {
        type: String,
    },
    countOfPersons: {
        type: Number,
        enum: [1, 2, 3, 4],
    },
    contact: {
        name: {
            first: String,
            last: String,
        },
        number: {
            mobile: String,
            phone: String,
        },
        email: String,
    },
    presentations: [{
        type: Object,
        enum: [{
            lesson: {
                type: Number,
                enum: [1, 2, 12, 3, 23, 4, 34],
            },
            type: {
                type: String,
                enum: ['short', 'long'],
            },
            branch: [{
                type: Object,
                enum: CONFIG.branches,
            }],
        }],
    }],
    accessories: [{
        type: Object,
        enum: CONFIG.presentationRequirments,
    }],
    additionalInfo: String,
    tour: Boolean,
    edits: [{
        date: {
            type: Date,
            default: moment(),
        },
        author: String, // User's _id
    }],
    dateOfCreation: {
        type: Date,
        default: moment(),
    },
    author: {
        type: String,
        default: `Formulář`,
    },
    year: {
        type: Number,
        default: CONFIG.year,
    },
});

// export
module.exports = mongoose.model('Company', companySchema, 'company');