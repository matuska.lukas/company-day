/*
 * Year database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// library for easy database manipulations
const mongoose = require('../libs/db');
const moment = require('moment');
moment.locale('cs');

// the schema itself
var yearSchema = new mongoose.Schema({
    author: String,
    name: Number,
    descriprion: String,
    date: {
        type: Date,
        default: moment(),
    },
});

// export
module.exports = mongoose.model('Year', yearSchema, 'year');