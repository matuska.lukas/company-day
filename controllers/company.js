/**
 * Company controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const bcrypt = require('bcrypt');
const moment = require('moment');
moment.locale('cs');
const randomstring = require('randomstring');
const nodemailer = require("nodemailer");
const fs = require('fs'); // File system

/**
 * Models
 */
const User = require('../models/User');
const Company = require('../models/Company');

module.exports.register = (req, res) => {
    if (!req.body.name) {
        return res.redirect('/company/register?err=missing-name');
    } else if (!req.body.address) {
        return res.redirect('/company/register?err=missing-address');
    } else if (!req.body.contactFirstname) {
        return res.redirect('/company/register?err=missing-firstname');
    } else if (!req.body.contactLastname) {
        return res.redirect('/company/register?err=missing-lastname');
    } else if (!req.body.contactMobile) {
        return res.redirect('/company/register?err=missing-mobile');
    } else if (!req.body.contactPhone) {
        return res.redirect('/company/register?err=missing-phone');
    } else if (!req.body.contactEmail) {
        return res.redirect('/company/register?err=missing-email');
    } else if (!req.body.countOfPerson) {
        return res.redirect('/company/register?err=missing-count-of-person');
    } else if (!req.body.lessons) {
        return res.redirect('/company/register?err=missing-lessons');
    } else if (!req.body.branches) {
        return res.redirect('/company/register?err=missing-branches');
    } else if (!req.body.tour) {
        return res.redirect('/company/register?err=missing-tour');
    }

    let company = {
        name: req.body.name,
        address: req.body.address,
        countOfPerson: req.body.countOfPerson,
        contact: {
            name: {
                first: req.body.contactFirstname,
                last: req.body.contactLastname,
            },
            number: {
                mobile: req.body.contactMobile,
            },
            email: req.body.contactEmail,
        },
        presentations: [],
        accessories: [],
        additionalInfo: req.body.additionalInfo,
    };

    if (req.body.contactPhone){
        company.contact.number.phone = req.body.contactPhone;
    }

    if (req.body.lessons.length > 0) {
        for (let i = 0; i < req.body.lessons.length; i++) {
            let presentation = {};
            switch (req.body.lessons[i]) {
                case 1:
                    presentation.lesson = 1;
                    presentation.type = 'short';
                    presentation.branch = CONFIG.branches[ii];
                    break;

                case 2:
                    presentation.lesson = 2;
                    presentation.type = 'short';
                    presentation.branch = CONFIG.branches[ii];
                    break;

                case 12:
                    presentation.lesson = 12;
                    presentation.type = 'long';
                    presentation.branch = CONFIG.branches[ii];
                    break;

                case 34:
                    presentation.lesson = 34;
                    presentation.type = 'long';
                    presentation.branch = CONFIG.branches[ii];
                    break;
            }

            for (let i = 0; i < req.body.branches.length; i++) {
                for (let ii = 0; ii < CONFIG.branches.length; ii++) {
                    if (CONFIG.branches[ii].short == req.body.branches[i]) {
                        presentation.branch.push(CONFIG.branches[ii]);
                    }
                }
            }
            company.presentations.push(presentation);
        }
    }

    if (req.body.accessories) {
        if (req.body.accessories.length > 0){
            for (let i = 0; i < req.body.accessories.length; i++) {
                for (let ii = 0; ii < req.body.accessories[i].length; ii++) {
                    if (CONFIG.accessories[ii].short == req.body.accessories[i]){
                        company.accessories.push(CONFIG.accessories[ii]);
                    }
                }
            }
        }
    }

    if (req.body.tour) {
        switch (req.body.tour) {
            case 'yes':
                company.tour = true;
                break;

            case 'no':
                company.tour = false;
                break;
        }
    }

    if (req.session.user) {
        company.author = req.session.user._id;
    }

    console.log(company);
    // save to Database
    new Company(company).save((err, company) => {
        if (err) {
            return console.error(err);
        }

        /**
         * Send email
         */

        // Create a SMTP transporter object
        let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

        // Message object
        let message = {
            from: `${CONFIG.nodemailer.senderInfo.name} <${CONFIG.nodemailer.senderInfo.email}>`,
            to: company.contact.name.first + ' ' + company.contact.name.last + ' <' + company.contact.email + '>',
            subject: 'Vaše registrace na Den firem',
            text: `Dobrý den,\n\nděkujeme za Vaši registraci na Den firem u nás na Purkyňce, pto jistotu Vám níže Vaši registraci zrekaptilujeme.\n\nNázev firmy: ${company.name}\nAdresa: ${company.address}\n\nKontaktní osoba: ${company.contact.name.first} ${company.contact.name.last}\nMobil: ${company.contact.number.mobile}\nTelefon: ${company.contact.number.phone}\nEmail: ${company.contact.email}\nhttps://${CONFIG.myDomain}/login\n\nBest regards,\nQuiz admin team`,
            //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
            //html: fs.readFileSync('../mail/forgot-password.html');
        };

        transporter.sendMail(message, (err, info, response) => {
            if (err) {
                console.log('Error occurred. ' + err.message);
                return process.exit(1);
            }
            //console.log(info);
            //console.log(response);
        });

        if (!req.originalUrl.includes('/admin')) {
            res.redirect('/company/register?status=ok');
        }

    });
};

module.exports.list = (req, res) => {
    Company.find({
        year: req.session.year,
    }, (err, companies) => {
        if (err) {
            res.send(err);
            return console.error(err);
        }
        res.render('admin/companies/list', {req, res, companies});
    });
};