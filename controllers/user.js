/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const bcrypt = require('bcrypt');
const moment = require('moment');
moment.locale('cs');
const randomstring = require('randomstring');
const nodemailer = require("nodemailer");
const fs = require('fs'); // File system

/**
 * Models
 */
const User = require('../models/User');
const Year = require('../models/Year');


// do the register from POSTed form
module.exports.registerMe = (req, res) => {
    let email = req.body.email.trim().toLowerCase();
    let username = req.body.username.trim().toLowerCase();
    User.findOne({
        $or: [
            {
                email: email,
            },
            {
                username: username,
            }
        ],
    }, (err, user) => {
        if (err)
            return console.error(err);

        if (user && user.username === username) {
            if (req.originalUrl.includes('admin')) {
                return res.redirect(`/admin/register/?err=usernameAlreadyExists&email=${email}&firstname=${req.body.firstname}&lastname=${req.body.lastname}`);
            } else {
                return res.redirect(`/register/?err=usernameAlreadyExists&email=${email}&firstname=${req.body.firstname}&lastname=${req.body.lastname}`);
            }
        }

        if (user && user.email === email) {
            if (req.originalUrl.includes('admin')) {
                return res.redirect(`/admin/register/?err=emailAlreadyExists&username=${username}&firstname=${req.body.firstname}&lastname=${req.body.lastname}`);
            } else {
                return res.redirect(`/register/?err=emailAlreadyExists&username=${username}&firstname=${req.body.firstname}&lastname=${req.body.lastname}`);
            }
        }

        new User({
            name: {
                first: req.body.firstname,
                last: req.body.lastname,
            },
            username,
            password: bcrypt.hashSync(req.body.password, 15), // don't go under 13!
            email,
            author: req.session.user ? req.session.user._id : null,
            photoUrl: req.body.profilePhotoUrl,
            years: (req.body.years ? req.body.years : [CONFIG.year]),
        }).save((err, user) => {
            if (err) {
                return console.error(err);
            }

            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

            // Message object
            let message = {
                from: `${CONFIG.nodemailer.senderInfo.name} <${CONFIG.nodemailer.senderInfo.email}>`,
                to: user.name.first + ' ' + user.name.last + ' <' + user.email + '>',
                subject: 'Registrace - Den firem',
                text: `Dobrý den,\n\nName: ${user.name.first} ${user.name.last}\nUsername: ${user.username}\nEmail: ${user.email}\nhttps://${CONFIG.myDomain}/login\n\nBest regards,\nQuiz admin team`,
                //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                //html: fs.readFileSync('../mail/forgot-password.html');
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.log('Error occurred. ' + err.message);
                    return process.exit(1);
                }
                //console.log(info);
                //console.log(response);
            });

            if (req.originalUrl.includes('admin')) {
                if (req.originalUrl.includes('users/new')) {
                    return res.redirect('/admin/users/list?status=new-ok');
                }
                return res.redirect('/admin/login?status=register-ok');
            } else {
                return res.redirect('/login?status=register-ok');
            }
        });
    });
};

// do the login
module.exports.login = (req, res) => {
    if (req.body.username && req.body.password) {
        User.findOne({
            username: req.body.username.trim().toLowerCase(),
        }, (err, user) => {
            if (err)
                return console.error(err);
            if (!user)
                if (!req.originalUrl.includes('admin')) {
                    return res.redirect('/login/?err=bad-username');
                } else {
                    return res.redirect('/admin/login/?err=bad-username');
                }

            // compare the passwords
            bcrypt.compare(req.body.password, user.password, (err, same) => {
                if (err)
                    return console.error(err);

                // if the password are not the same, return message
                if (!same) {
                    // compare the passwords
                    bcrypt.compare(req.body.password, user.rescuePassword, (err, sameRescue) => {
                        if (err)
                            return console.error(err);
                        if (!sameRescue) {
                            if (!user.permissions.includes('admin'))
                                return res.redirect('/login?err=badPassword');
                            else
                                return res.redirect(`/admin/login?err=badPassword&username=${user.username}`);
                        }
                    });
                }

                User.findOneAndUpdate({
                    username: user.username,
                }, {
                        $push: {
                            logins: {
                                time: moment(),
                                ip: req.headers['x-forwarded-for'],
                            },
                        }
                    }, (err, user) => {
                        if (err)
                            console.error(err);

                        req.session.user = user;
                        if (user.years.includes(CONFIG.year)) {
                            req.session.year = CONFIG.year;
                        } else if (user.years.length-1 > 0) {
                            req.session.year = user.years[user.years.length-1].name;
                        }
                        req.session.loginTime = user.logins[user.logins.length - 1];
                        if (req.originalUrl.includes('admin'))
                            return res.redirect('/admin');
                        return res.redirect('/game');
                    });
            });
        });
    } else {
        res.send('bad-login-form');
    }
};

module.exports.list = (req, res) => {
    User.find({}, (err, users) => {
        if (err)
            return console.error(err);
        if (users) {
            for (let i = 0; i < users.length; i++) {
                if (users[i].author) {
                    User.findOne({
                        _id: users[i].author,
                    }, (err, user) => {
                        if (err)
                            return console.error(err);
                        // try
                        users[i].author = `${user.name.first} ${user.name.last}`;
                    });
                }
            }
            setTimeout(function () {
                return res.render('admin/users/list', { req, res, users });
            }, 100);
        }
    })
};

module.exports.edit = (req, res) => {
    if (req.body.id) {
        //console.log(req.body);
        User.updateOne({
            _id: req.body.id,
        }, {
                name: {
                    first: req.body.firstname,
                    last: req.body.lastname,
                },
                masterAdmin: req.body.masterAdmin,
                ail: req.body.email,
                username: req.body.username,
                photoUrl: req.body.profilePhotoUrl,
                permissions: req.body.permissions,
            }, (err, user) => {
                if (err)
                    return console.error(err);
                res.redirect('/admin/users/list/?status=user-edit-ok');
            }
        );
    }
};

module.exports.update = (req, res) => {
    User.findOne({
        _id: req.session.user._id,
    }, (err, user) => {
        if (err)
            return console.error(err);
        if (req.body.password) {
            // compare the passwords
            bcrypt.compare(req.body.password, user.password, (err, same) => {
                if (err)
                    return console.error(err);

                // if the password are not the same, return message
                if (!same)
                    return res.redirect('/login?err=badPassword');

                if (req.body.firstname)
                    user.name.first = req.body.firstname;
                if (req.body.lastname)
                    user.name.last = req.body.lastname;
                if (req.body.username)
                    user.username = req.body.username;
                if (req.body.email)
                    user.email = req.body.email;
                req.session.user = user;
                user.save((err) => {
                    if (err)
                        return console.error(err);
                    res.redirect('/admin/profile?err=ok');
                });
            });
        } else {
            res.redirect('/admin/profile?err=passwordWasntProvided');
        }
    });
};

module.exports.changePassword = (req, res) => {
    if (req.body.oldPassword && req.body.newPassword) {
        // compare the passwords
        bcrypt.compare(req.body.oldPassword, req.session.user.password, (err, same) => {
            if (err)
                return console.error(err);

            // if the password are not the same, return message
            if (!same)
                return res.redirect('/admin/profile?err=badPassword');

            User.findOneAndUpdate({
                _id: req.session.user._id,
            }, {
                    $set: {
                        password: bcrypt.hashSync(req.body.newPassword, 15),
                    },
                }, {
                    new: true,
                }, (err, user) => {
                    if (err)
                        return console.error(err);
                    req.session.user = user;
                    res.redirect('/admin/profile?err=ok');
                });
        });
    } else {
        res.redirect('/admin/profile?err=passwordWasntProvided');
    }
};

module.exports.forgotPassword = (req, res) => {
    User.findOne({
        username: req.body.username,
    }, (err, user) => {
        if (err) {
            return console.error(err);
        } else if (user) {
            let newPassword = randomstring.generate();
            user.rescuePassword = bcrypt.hashSync(newPassword, 15)
            user.save((err) => {
                if (err)
                    return console.error(err);
                // Create a SMTP transporter object
                let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

                // Message object
                let message = {
                    from: `${CONFIG.nodemailer.senderInfo.name} <${CONFIG.nodemailer.senderInfo.email}>`,
                    to: user.name.first + ' ' + user.name.last + ' <' + user.email + '>',
                    subject: 'Your forget password 🔒',
                    text: `Hello,\there is your new password: ${newPassword}\nLogon with this password on https://${CONFIG.myDomain}/login\n\nHave a nice day,\nQuiz admin team`,
                    //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                    //html: fs.readFileSync('../mail/forgot-password.html');
                };

                transporter.sendMail(message, (err, info, response) => {
                    if (err) {
                        console.log('Error occurred. ' + err.message);
                        return process.exit(1);
                    }
                    //console.log(info);
                    //console.log(response);
                });
                res.render('admin/forgotPasswordOk', { req, res, user });
            });
        } else {
            res.redirect('/admin/login/forgot-password?err=not-existing-username');
        }
    });
};

module.exports.setNewPassword = (req, res) => {
    if (!req.body.id) {
        return res.send('not-send-id');
    } else if (!req.body.newPassword) {
        return res.send('not-send-password');
    } else {
        //console.log('Setting new password!');
        User.findOne({
            _id: req.body.id,
        }, (err, user) => {
            if (err) {
                return console.error(err);
            }
            //console.log(req.body.newPassword);
            user.password = bcrypt.hashSync(req.body.newPassword, 15)
            user.save((err) => {
                if (err)
                    return console.error(err);
                // Create a SMTP transporter object
                let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

                // Message object
                let message = {
                    from: `${CONFIG.nodemailer.senderInfo.name} <${CONFIG.nodemailer.senderInfo.email}>`,
                    to: user.name.first + ' ' + user.name.last + ' <' + user.email + '>',
                    subject: 'Vaše nové heslo 🔑',
                    text: 'Dobrý den,\nzde je vaše heslo: ' + req.body.newPassword + '\nNyní se můžete přihlásit s tímto heslem. https://pohles.rudickamladez.cz/login\n\nS přáním hezkého dne,\nPořadatelé Pohádkového lesa v Rudici',
                    //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                    //html: fs.readFileSync('../mail/forgot-password.html');
                };

                transporter.sendMail(message, (err, info, response) => {
                    if (err) {
                        console.log('Error occurred. ' + err.message);
                        return process.exit(1);
                    }
                    //console.log(info);
                    //console.log(response);
                });
                //res.render('admin/forgotPasswordOk', { req, res, user });
                res.send('ok');
            });
        });
    }
};

module.exports.changePathOfProfilePhoto = (req, res) => {
    if (req.body.newProfilePhotoLink) {
        User.findOne({
            _id: req.session.user._id,
        }, (err, user) => {
            if (err) {
                return console.error(err);
            }
            if (req.body.newProfilePhotoLink.includes('http:')) {
                req.body.newProfilePhotoLink = req.body.newProfilePhotoLink.split('http:')[1].trim();
            } else if (req.body.newProfilePhotoLink.includes('https:')) {
                req.body.newProfilePhotoLink = req.body.newProfilePhotoLink.split('https:')[1].trim();
            }
            user.photoUrl = req.body.newProfilePhotoLink;
            req.session.user = user;
            user.save((err) => {
                if (err)
                    return console.error(err);
                res.redirect('/admin/profile/?err=newProfilePhotoLinkOk');
            });
        });
    } else {
        res.redirect('/admin/profile/?err=newProfilePhotoLinkWasntProvided');
    }
};

module.exports.changeProfilePhoto = (req, res) => {
    if (req.file) {
        User.findOne({
            _id: req.session.user._id,
        }, (err, user) => {
            if (err) {
                return console.error(err);
            }
            user.photoUrl = req.file.path.slice(6, req.file.path.length);
            req.session.user = user;
            user.save((err) => {
                if (err)
                    return console.error(err);
                res.redirect('/admin/profile/?err=newProfilePhotoOk');
            });
        });
    } else {
        res.redirect('/admin/profile/?err=newProfilePhotoWasntProvided');
    }
};

module.exports.showAdminProfile = (req, res) => {
    return res.render('admin/profile', { req, res });
};
module.exports.showEdit = (req, res) => {
    Year.find({}, (err, years) => {
        if (err) {
            res.send(err);
            return console.error(err);
        }
        User.findById(req.query.id, (err, user) => {
            if (err) {
                res.send(err);
                return console.error(err);
            }
            res.render('admin/users/edit', { req, res, user, years });
        });
    });
};

module.exports.showNew = (req, res) => {
    res.render('admin/users/new', { req, res });
};


// delete the user
module.exports.delete = (req, res) => {
    if (req.body.id) {
        User.findByIdAndDelete(req.body.id, (err) => {
            if (err)
                return console.error(err);
            res.send('ok');
        });
    } else {
        res.send('not-sent-id');
    }
};

// logout the user
module.exports.logout = (req, res) => {
    if (!req.session.user.permissions.includes('admin')) {
        req.session.destroy();
        return res.redirect('/login/?err=logout-ok');
    } else {
        req.session.destroy();
        return res.redirect('/admin/login/?err=logout-ok');
    }
};

module.exports.updateSessions = (req, res) => {
    setTimeout(() => {
        User.findOne(
            {
                _id: req.session.user._id,
            }, (err, user) => {
                if (err) {
                    return console.error(err);
                }
                if (user) {
                    //console.log(user);
                    req.session.user = user;
                } else {
                    //res.redirect('/admin/logout');
                    res.send('fail');
                }
            }
        );
    }, 200);
};