/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');

/**
 * Models
 */
const User = require('../models/User');

module.exports.loginPage = (req, res) => {
    res.render('login/login', {req, res});
};

module.exports.forgetPasswordPage = (req, res) => {
    res.render('login/forgot-password', {req, res});
};

module.exports.registerPage = (req, res) => {
    res.render('login/register', {req, res});
};

module.exports.registerPageForCompanies = (req, res) => {
    res.render('company/register', {req, res});
};

module.exports.adminLoginPage = (req, res) => {
    randomLoves = ['$ocky', 'tupý grafiky', '€lektrikáře'];
    let randomLove = randomLoves[Math.floor(Math.random()*randomLoves.length)];
    res.render('admin/login/login', {req, res, randomLove});
};

module.exports.adminRegisterPage = (req, res) => {
    randomLoves = ['$ocky', 'tupý grafiky', '€lektrikáře'];
    let randomLove = randomLoves[Math.floor(Math.random()*randomLoves.length)];
    res.render('admin/login/register', {req, res, randomLove});
};
