/**
 * Year controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');

/**
 * Models
 */
const Year = require('../../models/Year');
const User = require('../../models/User');

module.exports.new = (req, res) => {
    Year.findOne({
        name: req.body.name,
    }, (err, year) => {
        if (err)
            console.error(err);
        if (year && year.name == req.body.name) {
            return res.redirect(`${req.originalUrl}?status=year-already-exists`);
        }

        new Year({
            author: req.session.user._id,
            name: req.body.name,
            description: req.body.description,
        }).save((err, year) => {
            if (err)
                return console.error(err);
            // add year to author ;)
            User.findByIdAndUpdate(req.session.user._id, (err, user) => {
                if (err) {
                    res.send(err);
                    return console.error(err);
                }
                user.years.push(year.name);
                req.session.user.years.push(year.name);
                res.redirect(`/admin/years/list?status=ok`);
            });
        });
    });
};

module.exports.list = (req, res) => {
    Year.find({}, (err, years) => {
        if (err)
            return console.error(err);
        for (let i = 0; i < years.length; i++) {
            if (years[i].author != "Formulář") {
                User.findOne({
                    _id: years[i].author,
                }, (err, user) => {
                    if (err)
                        console.error(err);
                    years[i].author = `${user.name.first} ${user.name.last}`;
                });
            }
            //console.log(reservations[i].author);
        }
        setTimeout(function () {
            return res.render('admin/years/list', { req, res, years });
        }, 100);
    });
};

module.exports.showNew = (req, res) => {
    res.render('admin/years/new', { req, res });
};

module.exports.showEdit = (req, res) => {
    Year.findOne({
        _id: req.query.id,
    }, (err, year) => {
        if (err) {
            return console.error(err);
        }
        res.render('admin/years/edit', { req, res, year });
    });
};

module.exports.edit = (req, res) => {
    console.log(req.body);
    if (req.body.id && req.body.name && req.body.times) {
        if (req.body.times[req.body.times.length - 1] == "") {
            req.body.times[req.body.times.length - 1] = null;
        }
        console.log(req.body);
        Year.updateOne({
            _id: req.body.id,
        }, {
                name: req.body.name,
                times: req.body.times,
            }, (err) => {
                if (err)
                    return console.error(err);
                res.redirect('/admin/years/list?status=edit-ok');
            }
        );
    }
};