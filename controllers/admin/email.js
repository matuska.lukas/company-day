/**
 * Email controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');
const nodemailer = require("nodemailer");

/**
 * Models
 */
const User = require('../../models/User');

module.exports.sendEmail = (req, res) => {
    // Create a SMTP transporter object
    let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);
/*
    if (req.body.recipient.includes(',')){
        let recipients = req.body.recipient.split(',');
        for (let i = 0; i < recipients.length; i++) {
            recipients[i] = recipients[i].trim();
        }
    }*/

    // Message object
    let message = {
        from: 'Pohádkový les Rudice <pohles@rudickamladez.cz>',
        to: req.body.recipient.trim(),
        subject: req.body.subject.trim(),
        text: req.body.text.trim(),
        //html: `<h1>Dobrý den,</h1>`
        //html: fs.readFileSync('../mail/forgot-password.html');
    };

    transporter.sendMail(message, (err, info, response) => {
        if (err) {
            console.error('Error occurred. ' + err.message);
            return process.exit(1);
        }
        //console.log(info);
        //console.log(response);
    });

    res.redirect('/admin/email/write?status=email-sent')
};