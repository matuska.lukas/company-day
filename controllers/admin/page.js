/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');

/**
 * Models
 */
const User = require('../../models/User');

module.exports.login = (req, res) => {
    randomLoves = ['$ocky', 'tupý grafiky', '€lektrikáře'];
    let randomLove = randomLoves[Math.floor(Math.random()*randomLoves.length)];
    res.render('admin/login/login', {req, res, randomLove});
};

module.exports.register = (req, res) => {
    randomLoves = ['$ocky', 'tupý grafiky', '€lektrikáře'];
    let randomLove = randomLoves[Math.floor(Math.random()*randomLoves.length)];
    res.render('admin/login/register', {req, res, randomLove});
};

module.exports.forgotPassword = (req, res) => {
    randomLoves = ['$ocky', 'tupý grafiky', '€lektrikáře'];
    let randomLove = randomLoves[Math.floor(Math.random()*randomLoves.length)];
    res.render('admin/login/forgot-password', {req, res, randomLove});
};

module.exports.dashboard = (req, res) => {
    res.render('admin/dashboard', {req, res});
};

module.exports.newUser = (req, res) => {
    res.render('admin/users/new', {req, res});
};