const mongoose = require('mongoose');
const saslprep = require('saslprep');
const moment = require('moment');
moment.locale('cs');

// connect to the database
mongoose.connect('mongodb://' + CONFIG.db.host + '/' + CONFIG.db.name, {
	auth: {
		authdb: CONFIG.db.name,
	},
	useUnifiedTopology: true,
	port: CONFIG.db.port,
	user: CONFIG.db.user,
	pass: CONFIG.db.password,
	useNewUrlParser: true,
}, (err) => {
    if (err)
    	return console.log(err);

    console.log(moment().format('YYYY-MM-DD HH:mm:ss') + ' Connected to database');
});

module.exports = mongoose;