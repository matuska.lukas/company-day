/**
 * The entry point of the Company day app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

// Config file as global variable
global.CONFIG = require('./config');

// Load the server plugin (Express)
const express = require('express');
const app = express();

// Load libraries
const moment = require('moment');
moment.locale('cs');
const path = require('path');
const bodyparser = require('body-parser');

// Session handling
const session = require('express-session');
const redis = require('redis');
const redisStore = require('connect-redis')(session);

// Connect to the redis server
const redisClient = redis.createClient();
const store = new redisStore({
    host: 'localhost', 
    port: 6379,
    client: redisClient,
    ttl: 86400,
});

// Set up the redis store to saving session data
app.use(session({
    secret: 'Company day ... the best day.',
    store: store,
    name: 'BSID',
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 86400000,
    },
}));


// set extended urlencoded to true (post)
app.use(bodyparser.urlencoded({extended: true}));

// set up views directory and the rendering engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// set serving static files from the static dir
app.use(express.static(path.join(__dirname, 'static')));

/**
 * Routers
*/

// API
const apiRouter = require('./routes/api');
app.use('/api', apiRouter);

// Admin section
const adminRouter = require('./routes/admin');
app.use('/admin', adminRouter);

// User site
const userSiteRouter = require('./routes/user-site');
app.use('/user-site', userSiteRouter);

// Others & Main web pages
const webRouter = require('./routes/web');
app.use('/', webRouter);

/**
 * Error handling
 */
app.use((req, res) => {
    res.status(400).send('Generic 400 error<br/>\n' + err);
});
/*
app.use((err, req, res, next) => {
    res.status(500).send('Generic server error<br/>\n' + err);
});*/

// run the server
app.listen(CONFIG.port, () => {
    console.log(moment().format('YYYY-MM-DD HH:mm:ss') + ' Listening on port ' + CONFIG.port + ' (Company day Node.js <3)');
});