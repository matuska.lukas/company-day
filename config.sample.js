const moment = require('moment');
moment.locale('cs');

module.exports = {
    // location
    protocol: 'http',
    url: 'localhost',
    port: 3002,

    // full url
    fullUrl: this.protocol + '://' + this.url + (String(this.port).length > 0 ? ':' + this.port : ''),

    // database credentials
    db: {
        port: 27017,
        host: 'localhost',
        name: 'companyday',
        user: 'companyday',
        password: '',
    },

    reCaptcha: {
        siteKey: '',
        secretKey: '',
    },

    year: 2019,

    reservations: {
        dateOfEnd: moment("19 October 2019 15:00", "DD MMMM YYYY HH:mm"),
        maxCountOfKidsForOneReservation: 8,
        maxCountOfKids: 15,
    },

    permissions: ['admin', 'masterAdmin', 'email',
    'users', 'users-edit', 'user-change-password',
    'years', 'years-edit',
    'reservations', 'reservations-delete-all', 'reservations-delete', 'reservations-new', 'reservations-edit', 'reservation-print', 'reservation-send-mail',
    'articles', 'articles-delete', 'articles-new', 'articles-edit',
    'references', 'references-delete', 'references-new', 'references-edit',
    'places', 'places-list', 'places-edit', 'places-new', 'places-detail',
    //'', '-delete', '-new', '-edit',
    /*'finance', 'documents',*/],

    nodemailer: {
        settings: {
            host: "smtp.example.com",
            port: 465,
            secure: true, // upgrade later with STARTTLS
            auth: {
                user: "sender@example.com",
                pass: "password"
            },
            tls: {
                // do not fail on invalid certs
                rejectUnauthorized: false,
            }
        },
        senderInfo: {
            name: "Den firem SPŠ Brno, Purkyňova",
            email: "denfirem@example.com",
        },
    }
};